﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class CreateGroup : Form
    {
        Listen listen;
        string text="";
        public CreateGroup(string txt,Listen lstn)
        {
            InitializeComponent();
            listen = lstn;
            text = txt;
        }

    
        private void button1_Click(object sender, EventArgs e)
        {
            var txtarr=text.Split('?');
            var txt = txtarr[0] + txtGroupName.Text + txtarr[1];
            listen.Send(txt);
            this.Close();
        }
    }
}
