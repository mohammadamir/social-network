﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
   public class Group
    {
        public string Name { get; set; }

        public string Admin { get; set; }

        public string Ip { get; set; }

        public string Port { get; set; }

        public string GropuUsers { get; set; }
    }
}
