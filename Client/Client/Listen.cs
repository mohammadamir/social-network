﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public class Listen
    {
        public Socket socket, udpsock, multisock;

        public bool IsConnect;

        public Listen()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            udpsock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            multisock=new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        }

        public delegate void ReceivedEventHandler(Listen listen, string receive);
        public event ReceivedEventHandler Received = delegate { };

        public delegate void DisconnectedEventHandler(Listen listen);
        public event DisconnectedEventHandler Disconnected = delegate { };

        public event EventHandler Connected = delegate { };

        //connect to endpoint ip and port in socket that create before 
        public void Connect(string ip, int port)
        {
            try
            {
                //udp socket run on new task
                var broadcastip = new IPEndPoint(IPAddress.Any, port);
                udpsock.Bind(broadcastip);

                Task.Run(() =>
                {
                    while (true)
                    {
                        var endpoint = (EndPoint)broadcastip;
                        var udpbuffer = new byte[udpsock.ReceiveBufferSize];
                        var udprec = udpsock.ReceiveFrom(udpbuffer, ref endpoint);
                        if (udprec != 0)
                        {
                            var data = Encoding.UTF8.GetString(udpbuffer,0,udprec);
                            //var data = Encoding.ASCII.GetString(udpbuffer, 0, udprec);
                            Received(this, data);
                        }
                        
                    }
                });
                //Main m = new Main();
                //if (m.List.Items.Count >= 0)
                //{
                //    //MultiCast
                //    IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 1212);
                //    multisock.Bind(ipep);
                //    IPAddress ip1 = IPAddress.Parse("224.5.6.7");
                //    multisock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip1, IPAddress.Any));
                //    Task.Run(() =>
                //    {
                //        while (true)
                //        {

                //            //MultiCast
                //            var eop = (EndPoint)ipep;
                //            var multibuffer = new byte[multisock.ReceiveBufferSize];
                //            var multirec = multisock.ReceiveFrom(multibuffer, ref eop);
                //            if (multirec != 0)
                //            {
                //                var str = Encoding.UTF8.GetString(multibuffer, 0, multirec);
                //                Received(this, str);
                //            }
                //        }
                //    });
                //}
             
                //TCP socket
                var ep = new IPEndPoint(IPAddress.Parse(ip), 2018);
                socket.BeginConnect(ep, ConnectCallback, socket);       //after connect run calback

            }
            catch(Exception e)
            {
                //MessageBox.Show(e.Message);
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            socket.EndConnect(ar);
            IsConnect = true;
            Connected(this, EventArgs.Empty);
            var buffer = new byte[socket.ReceiveBufferSize];
            socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
        }

        private void ReadCallback(IAsyncResult ar)
        {
            var buffer = (byte[])ar.AsyncState;
            var rec = socket.EndReceive(ar);
            if (rec != 0)
            {
                var data = Encoding.UTF8.GetString(buffer, 0, rec);
                //var data = Encoding.ASCII.GetString(buffer, 0, rec);
                Received(this, data);   //recieve event
            }
            else
            {
                Disconnected(this);
                IsConnect = false;
                Close();
                return;
            }
            socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
        }

        public void Close()
        {
            socket.Dispose();
            socket.Close();
        }

        //send all data to server
        public void Send(string data)
        {
            try
            {
                var buffer = Encoding.UTF8.GetBytes(data);
                socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, SendCallback, buffer);
            }
            catch(Exception e)
            {
                Disconnected(this);
                //MessageBox.Show(e.Message);
            }

        }

        private void SendCallback(IAsyncResult ar)
        {
            socket.EndSend(ar);
        }
    }
}
