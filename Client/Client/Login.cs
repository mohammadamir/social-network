﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Login : Form
    {
        public string TextIP
        {
            get
            {
                return txtIP.Text;
            }
            set
            {
                txtIP.Text = value;
            }
        }

        public string TextUserName
        {
            get
            {
                return txtUserName.Text;
            }
            set
            {
                txtUserName.Text = value;
            }
        }
        public int TextPort
        {
            get
            {
                return Convert.ToInt32(txtPort.Text);
            }
        }
        public Listen listen=new Listen();  
        public Login()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            listen.Connected += Client_Connected;
            listen.Connect(txtIP.Text, TextPort);
            listen.Send(txtUserName.Text + "|ConnectReq|" + TextPort+"|Connected" );
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            this.Invoke(Close);
        }
    }
}
