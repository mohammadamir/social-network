﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Main : Form
    {
        Login login=new Login();
        Listen listen;
        public Main()
        {
            InitializeComponent();
            lblYou.Text = "";
            lblTo.Text = "";
        }
        protected override void OnLoad(EventArgs e)
        {
            login.listen.Received += Client_Received;
            login.listen.Disconnected += Client_Disconnected;
            
            //Text += "TCP Chat - " + loginform.TextIP + " - (Connected as: " + loginform.TextUserName + " )";
            login.ShowDialog();
            lblUsername.Text = login.TextUserName;
            lblIP.Text = login.TextIP;
            lblPort.Text = login.TextPort.ToString();
            lblStatus.Text = login.listen.IsConnect ? "Connect" : "Disconnect";
            base.OnLoad(e);
        }
        /*private void Main_Load(object sender, EventArgs e)
        {
            base.OnLoad(e);
            login.listen.Received += Client_Received;
            login.listen.Disconnected += Client_Disconnected;
            lblUsername.Text = login.TextUserName;
            lblIP.Text = login.TextUserName;
            lblPort.Text = login.TextPort.ToString();
            lblStatus.Text = login.listen.IsConnect?"Connect":"Disconnect";
            lblUsername.Text = login.TextUserName;

            //Text += "TCP Chat - " + loginform.TextIP + " - (Connected as: " + loginform.TextUserName + " )";
            login.ShowDialog();
        }*/

        private void Client_Disconnected(Listen listen)
        {
            lblStatus.Text = "Disconnect";
        }

        private void Client_Received(Listen listen, string receive)
        {
            var text = receive.Split('|');
            switch (text[1])
            {
                case "ConnectReqAck":
                    this.Invoke(() =>
                    {
                        userList.Items.Clear();
                        for (int i = 2 ; i < text.Length; i++)
                        {
                            //var listview = new ListViewItem();
                            //listview.SubItems.Add(text[i]);
                            userList.Items.Add(text[i]);
                            //listview.Tag= login.listen.socket;
                            //userList.Items.Add(listview);
                        }
                    });

                    break;
                case "DisconnectReq":
                    this.Invoke(() =>
                    {
                        userList.Items.Clear();
                        for (int i = 2; i < text.Length; i++)
                        {
                            userList.Items.Add(text[i]);
                        }
                    });
                    break;
                case "PrivateChat":
                    this.Invoke(() =>
                    {
                        GetPrivateChat(text[2], text[3], text[4]);
                    });
                    break;

                case "CreateGroupAck":
                    this.Invoke(() =>
                    {
                        var group = new Group { Admin = text[3], Name = text[2], Ip = text[4], Port = text[5] };
                        var item = new ListViewItem();    
                        item.SubItems.Add(text[2]);
                        item.Text = text[2];
                        item.Tag = group;
                        groupList.Items.Add(item);
                        //for (int i = 0; i < groupList.Items.Count; i++)
                        //{
                        //    var client = groupList.Items[i].t as Client;

                        //}
                    });
                    break;

                case "JoinToGroup":
                    this.Invoke(() =>
                    {
                        var result = MessageBox.Show("Do you want to join in " + text[2], "Request Join To Group For " + lblUsername.Text, MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            var group = new Group { Admin = text[3], Name = text[2], Ip = text[4], Port = text[5] };
                            var item = new ListViewItem();
                            item.SubItems.Add(text[2]);
                            item.Text = text[2];
                            item.Tag = group;
                            groupList.Items.Add(item);
                            login.listen.Send(lblUsername.Text + "|JoinToGroupAck|" + group.Name);
                        }
                    });
                    break;

                case "NewUserJoinToGroup":
                    this.Invoke(() =>
                    {
                        for (int i = 0; i < groupList.Items.Count; i++)
                        {
                            if (groupList.Items[i].Text == text[2])
                            {
                                var g = groupList.Items[i].Tag as Group;
                                g.GropuUsers = "";
                                g.GropuUsers = text[3];

                            }
                        }
                    });
                    break;

                case "GroupChat":
                    this.Invoke(() =>
                    {
                        //GetMultiCastGroup();
                    });
                   break;
                default:
                    break;
            }
        }

       
        private void userList_DoubleClick(object sender, EventArgs e)
        {
            string dstusername = (string)userList.SelectedItem;
            if (dstusername != null)
            {
                lblYou.Text = login.TextUserName;
                lblTo.Text = dstusername;
                //PrivateChat pc = new PrivateChat(login.listen);
            }
        }
        private void btnSend_Click_1(object sender, EventArgs e)
        {
            var a = false;
            if (txtInput.Text != string.Empty)
            {
                   for (int i = 0; i < userList.Items.Count; i++)
                    {
                        if ((string)userList.Items[i] == lblTo.Text)
                        {
                            var t = lblYou.Text + "|PrivateChat|" + lblTo.Text + "|" + txtInput.Text;
                            login.listen.Send(t);   //
                            var newchat = "You To " + lblTo.Text + " : " + txtInput.Text + "\r\n";
                            //txtPChat.Clear();
                            txtPChat.Text += newchat;
                            txtInput.Text = string.Empty;
                            a = true;
                        }
                    }
                
                 if (!a)
                {
                    for (int k = 0; k < groupList.Items.Count; k++)
                    {
                        if (groupList.Items[k].Text == lblTo.Text)
                        {
                            var item = groupList.Items[k].Tag as Group;
                            Socket multicast = new Socket(AddressFamily.InterNetwork,SocketType.Dgram, ProtocolType.Udp);
                            IPAddress ip = IPAddress.Parse(item.Ip);
                            multicast.SetSocketOption(SocketOptionLevel.IP,SocketOptionName.AddMembership, new MulticastOption(ip));
                            multicast.SetSocketOption(SocketOptionLevel.IP,SocketOptionName.MulticastTimeToLive, 2);
                            IPEndPoint ipep = new IPEndPoint(ip, Convert.ToInt32(item.Port));
                            multicast.Connect(ipep);
                            var buffer = Encoding.UTF8.GetBytes(lblYou+"|GroupChat|"+ txtInput.Text);
                            multicast.Send(buffer,buffer.Length, SocketFlags.None);
                            multicast.Close(); 
                        }
                    }
                }
            }
        }


        private void GetPrivateChat(string srcuname,string destuname,string text)
        {
            if (srcuname != destuname)
            {
                var newchat = srcuname + " To You : " + text + "\r\n";
                this.Invoke(() =>
                {
                    txtPChat.Text += newchat;
                });
            }
            
        }

        private void createGroupToolStripMenuItem_Click(object sender, EventArgs e)
            {
            string guser = lblUsername.Text + "|CreateGroup|?|Users|";
            foreach (string item in userList.SelectedItems)
            {
                if(item!=lblUsername.Text)
                guser += item + "|";
            }
            CreateGroup cg = new CreateGroup(guser.TrimEnd('|'), login.listen);
            cg.ShowDialog();
        }

        private void groupList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var dstusername = groupList.SelectedItems[0];
            if (dstusername != null)
            {
                lblYou.Text = login.TextUserName;
                lblTo.Text = dstusername.Text;
                lblGroup.Text = dstusername.Text + " Join: ";
                var g=dstusername.Tag as Group;
                var txtuser = g.GropuUsers.Split(':');
                var users = "";
                for (int i = 0; i < txtuser.Length; i++)
                {
                    users += txtuser[i]+" - ";
                }
                lblGroupUsers.Text = users.TrimEnd('-');
                lblGroupUsers.Visible = true;
                lblGroup.Visible = true;
            }
        }

        //private void Main_Load(object sender, EventArgs e)
        //{

        //}

        public ListView List
        {
            get
            {
                return groupList;
            }
            set
            {
                this.List = value;
            }
        }
    }
}
