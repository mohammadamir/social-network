﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    class Client
    {
        public string Username { get; set; }

        public IPEndPoint Ip { get; set; }

        public Socket Socket { get; set; }

        public delegate void ClientReceivedHandler(Client sender, byte[] data);
        public event ClientReceivedHandler Received;

        public delegate void ClientDisconnectedHandler(Client sender);
        public event ClientDisconnectedHandler Disconnected;


        public Client(Socket s)
        {
            Socket = s;
            Ip = (IPEndPoint)Socket.RemoteEndPoint;
            Socket.BeginReceive(new byte[] { 0 }, 0, 0, 0, Callback, null);
        }

        private void Callback(IAsyncResult ar)
        {
            try
            {
                Socket.EndReceive(ar);
                var buffer = new byte[Socket.ReceiveBufferSize];
                var rec = Socket.Receive(buffer, buffer.Length, 0);
                if (rec < buffer.Length)
                {
                    Array.Resize(ref buffer, rec);
                }
                if (Received != null)
                {
                    Received(this, buffer);
                }
                Socket.BeginReceive(new byte[] { 0 }, 0, 0, 0, Callback, null);
            }
            catch (Exception e)
            {
                Close();
                if (Disconnected != null)
                    Disconnected(this);
                //MessageBox.Show(e.Message);
            }
        }

        public void Close()
        {
            Socket.Dispose();
            Socket.Close();
        }
        public void Send(string data, Socket socket)
        {
            try
            {
                var buffer = Encoding.UTF8.GetBytes(data);
                //var buffer = Encoding.ASCII.GetBytes(data);
                socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, ar => socket.EndSend(ar), buffer);
            }
            catch (Exception e)
            {

            }
        }
        

    }
}
