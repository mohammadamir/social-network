﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    class Listener
    {
        int Port { get; set; }

        Socket socket { get; set; }

        bool Listening = false;


        /*Create TCP socket in Server and set port*/
        public Listener(int port)
        {
            Port = port;
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public delegate void SocketAcceptedHandler(Socket e);
        public event SocketAcceptedHandler SocketAccepted;

        public void Start()
        {
            if (Listening)
                return;
            socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), Port));
            socket.Listen(6);
            socket.BeginAccept(new AsyncCallback(Callback), null);    //callback in thread for everytime
            Listening = true;
        }

        public void Stop()
        {
            if (!Listening)
                return;
            if (socket.Connected)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close(1);    //after 1 sec close the connection
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);   //?????
            }
        }

        private void Callback(IAsyncResult ar)
        {
            try
            {
                var s = socket.EndAccept(ar);
                if(SocketAccepted!=null)
                    SocketAccepted(s);
                socket.BeginAccept(Callback, null);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
            }
        }
    }
}
