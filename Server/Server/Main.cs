﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{

    public partial class Main : Form
    {
        Listener listener;
        HashSet<UserInfo> usernames = new HashSet<UserInfo>();
        List<Group> groups = new List<Group>();

        public Main()
        {
            InitializeComponent();
            listener = new Listener(2018);
            listener.SocketAccepted += listener_SocketAccepted;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            listener.Start();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            listener.Stop();
        }

        private void listener_SocketAccepted(Socket s)
        {
            var client = new Client(s);
            client.Received += client_Received;
            client.Disconnected += client_Disconnected;
            this.Invoke(() =>
            {
                var ip = client.Ip.ToString().Split(':')[0];
                var item = new ListViewItem();    //set ip to list
                item.SubItems.Add(ip);
                item.SubItems.Add(" ");     //username
                item.SubItems.Add(" ");     //status
                item.SubItems.Add(" ");     //status
                item.Tag = client;
                clientList.Items.Add(item);
                //clients.Add(e);
            });
        }

        private void client_Disconnected(Client sender)
        {
            this.Invoke(() =>
            {
                for (int i = 0; i < clientList.Items.Count; i++)
                {
                    var client = clientList.Items[i].Tag as Client;
                    if (client.Ip == sender.Ip)
                    {
                        txtReceive.Text += "***** " + clientList.Items[i].SubItems[0].Text + " left Chat Service *****\r\n";
                        //Services serv = new Services(clientList);
                        string users = string.Empty;
                        usernames.Remove(new UserInfo { Username = client.Username });
                        clientList.Items.RemoveAt(i);
                        //send all users to client
                        for (int k = 0; k < clientList.Items.Count; k++)
                        {
                            users += clientList.Items[k].SubItems[0].Text + "|";
                        }
                        BroadCastData("Server|DisconnectReq|" + users.TrimEnd('|'));
                        
                    }
                }
            });
        }

    private void client_Received(Client sender, byte[] data)
        {
            this.Invoke(() =>
            {
                var txt = Encoding.UTF8.GetString(data);
                /*Encoding.ASCII.GetString(data)*/
                ;
                var text = txt.Split('|');
                if (!usernames.Contains(new UserInfo { Username = text[0] }))
                {
                    for (int i = 0; i < clientList.Items.Count; i++)
                    {
                        var client = clientList.Items[i].Tag as Client;
                        if (client == null || client.Ip != sender.Ip) continue;
                        //Services serv = new Services(clientList);
                        switch (text[1])
                        {
                            case "ConnectReq":
                                txtReceive.Text += "***** " + text[0] + " Join to Chat Service *****\r\n";
                                clientList.Items[i].SubItems[0].Text = text[0]; //set username
                                clientList.Items[i].SubItems[2].Text = text[2]; //set connection port
                                clientList.Items[i].SubItems[3].Text = text[3]; //set status        //set ip and port in ...
                                usernames.Add(new UserInfo {Username = text[0], Port=Convert.ToInt32(text[2]) });     //add username to usernams hashset
                                string users = string.Empty;
                                //send all users to client
                                for (int k = 0; k < clientList.Items.Count; k++)
                                {
                                    users += clientList.Items[k].SubItems[0].Text + "|";
                                }
                              BroadCastData("Server|ConnectReqAck|" + users.TrimEnd('|'));
                                break;
                            case "PrivateChat":
                                for (int k = 0; k < clientList.Items.Count; k++)
                                {
                                    if (clientList.Items[k].SubItems[0].Text == text[2])
                                    {
                                        var c = clientList.Items[k].Tag as Client; 
                                        txtReceive.Text += text[0] + " to " + text[2] + " : " + text[3]+"\r\n";
                                        client.Send("Server|PrivateChat|" + text[0] + "|"+ text[2]+"|"+ text[3], c.Socket);//Server|PrivateChat|SrcClientusername|DestClientusername|text
                                    }
                                }
                                break;
                            case "CreateGroup":
                                if (!groups.Any(t => t.Name == text[2])&&!usernames.Any(t=>t.Username==text[2]))
                                {
                                    var res = new Group
                                    {
                                        Name = text[2],
                                        Admin = text[0],
                                        Ip = "224.5.6.7",
                                        Port = "1212",
                                        GropuUsers=text[0]+":",
                                    };
                                    groups.Add(res);
                                    client.Send("Server|CreateGroupAck|"+res.Name+"|"+ res.Admin+"|" + res.Ip + "|" + res.Port+"|"+ res.GropuUsers, client.Socket);
                                    for (int h = 4; h < text.Length; h++)
                                    {
                                        for (int k = 0; k < clientList.Items.Count; k++)
                                        {
                                            if (clientList.Items[k].SubItems[0].Text == text[h])
                                            {
                                                var c = clientList.Items[k].Tag as Client;
                                                client.Send("Servar|JoinToGroup|" + res.Name + "|" + res.Admin + "|" + res.Ip + "|" + res.Port + "|" + res.GropuUsers, c.Socket);
                                            }
                                        }
                                    }
                                }

                                break;

                            case "JoinToGroupAck":
                                var g=groups.FirstOrDefault(t => t.Name == text[2]);
                                g.GropuUsers += text[0] + ":";
                                if (g != null)
                                {
                                    var gusers = g.GropuUsers.Split(':');
                                    if (gusers != null)
                                    {
                                        for (int h = 0; h < gusers.Length; h++)
                                        {
                                            for (int k = 0; k < clientList.Items.Count; k++)
                                            {
                                                if (clientList.Items[k].SubItems[0].Text == gusers[h])
                                                {
                                                    var c = clientList.Items[k].Tag as Client;
                                                    client.Send("Servar|NewUserJoinToGroup|" + g.Name + "|" + g.GropuUsers.TrimEnd(':'), c.Socket);
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                break;
                            default:
                                break;
                        }
                    }
                }
                //else
                //{
                //    //send that username is repeatedly
                //    return;
                //}
            });
        }

        public void BroadCastData(string data)
        {
            Task.Run(() =>
            {
                var udps = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                foreach (var item in usernames)
                {
                    var broadcastip = new IPEndPoint(IPAddress.Broadcast, item.Port);
                    udps.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                    try
                    {
                        var buffer = Encoding.UTF8.GetBytes(data);
                        //var buffer = Encoding.ASCII.GetBytes(data);
                        var a = udps.SendTo(buffer, broadcastip);
                        //udpsock.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, SendCallback, buffer);
                    }
                    catch (Exception e)
                    {

                    }
                }
            });
        }

        public void CreateGroup(string admin, string groupname)
        {
           
        }

      
    }
}
